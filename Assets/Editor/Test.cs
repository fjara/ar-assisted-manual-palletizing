﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;


using System.Xml;
public class Test
{
    XmlNodeList levelsList;
    [Test]
    public void ReadListFromXml_Test()
    {
        XmlDocument xmlDoc = new XmlDocument(); 
        xmlDoc.Load("Assets/Resources/XMLFiles/edit.xml");
        levelsList = xmlDoc.GetElementsByTagName("ImageTarget"); 

        BoxTest(new Vector3(0.23f, 0.05f, 0.15f), GetSizeFromXML("k1"));
        BoxTest(new Vector3(0.16f, 0.04f, 0.11f), GetSizeFromXML("k2"));
        BoxTest(new Vector3(0.10f, 0.10f, 0.10f), GetSizeFromXML("k3"));
        BoxTest(new Vector3(0.12f, 0.07f, 0.08f), GetSizeFromXML("k4"));
        BoxTest(new Vector3(0.14f, 0.08f, 0.08f), GetSizeFromXML("k6"));
        BoxTest(new Vector3(0.15f, 0.15f, 0.15f), GetSizeFromXML("k7"));

    }

    public void BoxTest(Vector3 ActualSize,  Vector3 Size)
    {
      
        Assert.AreEqual((int)ActualSize.x * 100, (int)Size.x * 100);
        Assert.AreEqual((int)ActualSize.y * 100, (int)Size.y * 100);
        Assert.AreEqual((int)ActualSize.z * 100, (int)Size.z * 100);
    }

    public Vector3 GetSizeFromXML(string name)
    {
        string nameRight = name + ".Right";
        string nameFront = name + ".Front";
        Vector3 size = new Vector3(0, 0, 0);
        foreach (XmlNode levelInfo in levelsList)
        {
            if (string.Equals(levelInfo.Attributes["name"].Value, nameRight))
            {
                string right = levelInfo.Attributes["size"].Value;
                string[] rights = right.Split(' ');
               size.y = float.Parse(rights[0])/1000000;
                size.z = float.Parse(rights[1]) / 1000000;
                
            }
            if (string.Equals(levelInfo.Attributes["name"].Value, nameFront))
            {
                string front = levelInfo.Attributes["size"].Value;
                string[] fronts = front.Split(' ');
                 size.x = float.Parse(fronts[0]) / 1000000;
     
            }

        }
        return size;
    }

}
