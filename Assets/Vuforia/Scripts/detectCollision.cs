﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectCollision : MonoBehaviour
{
    MainScript mainScript;

    void OnCollisionStay(Collision collision)
    {
        if (this.GetComponent<Collider>().bounds.Contains(collision.collider.bounds.max) && this.GetComponent<Collider>().bounds.Contains(collision.collider.bounds.min))
            mainScript.RegisterCollision(collision.gameObject.name);

    }

    // Start is called before the first frame update
    void Start()
    {
        mainScript = GameObject.Find("ARCamera").GetComponent<MainScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
