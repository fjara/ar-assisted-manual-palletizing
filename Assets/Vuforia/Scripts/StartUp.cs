﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartUp : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NormalButtonClicked()
    {
        SceneManager.UnloadSceneAsync("MainMenu");
        SceneManager.LoadScene("NormalMode");
       
    }

    public void RandomButtonClicked()
    {
        SceneManager.UnloadSceneAsync("MainMenu");
        SceneManager.LoadScene("RandomMode");
        
    }

    public void ExitButtonClicked()
    {
        Application.Quit();
    }
}
