﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : DefaultTrackableEventHandler
{
    // Start is called before the first frame update
    MainScript mainScript;
    protected override void Start()
    {
        base.Start();
        mainScript = GameObject.Find("ARCamera").GetComponent<MainScript>();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnTrackingFound()
    {

        mainScript.RegisterFound(mTrackableBehaviour.TrackableName); // prijavi glavnoj skripti da je detektovana kutija
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Enable rendering:
        foreach (var component in rendererComponents)
          component.enabled = true;
           

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
        {
           
              component.enabled = true;
        }
    }


    protected override void OnTrackingLost()
    {
        if(mainScript!=null)
             mainScript.RegisterLost(mTrackableBehaviour.TrackableName); // prijavi glavnoj skripti da je izgubljena kutija
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
        {
            component.enabled = true;
            //Debug.Log("aaaaaaaaaaaaaaaaaaaaaaaaaa");
        }

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;
    }


}
