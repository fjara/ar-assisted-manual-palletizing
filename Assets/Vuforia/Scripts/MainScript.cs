﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Xml;
using Vuforia;
using UnityEngine.SceneManagement;
//adding to gitlab https://stackoverflow.com/questions/21573405/how-to-prepare-a-unity-project-for-git

//ios build https://forum.unity.com/threads/creating-ar-app-for-ios-on-windows-pc.639304/
public class MainScript : MonoBehaviour
{

    /* Struktura koja cuva potrebne informacije za Trackable objekat*/
    private class Info
    {
        public Vector3 dimensions;
        public bool detected;
        public Vector3 position;
    }

  
   /* Moguca stanja u kojima moze da se nadje sistem*/
    public enum State   
    {
        WAITING_TRACKABLE = 0, // Stanje u kojem se ceka da se prepozna kutija
        DETECTED=1, // Medjustanje kroz koje se prolazi nakon detekcije kutije
        MATCHING_POSITION=2, // Stanje u kojem se ceka poklapanje polozaja kutije i pozicije na koju je treba smestiti
        NEXT_TRACKABLE=3, // Stanje u kojem se ceka pritisak na dugme, kako bi sistem bio spreman da ceka detekciju sledece kutije
        INIT=4 // inicijalno stanje, prisutno na pocetku dok se ne detektuje paleta
    }

    // Materijali koji sluze za polje
    public Material notPlaced;
    public Material placed;


    //Za cuvanje trenutnog stanja
    public State state;

    //Referenca na objekat koji sluzi za prikazivanje pozicije na koju treba smestiti kutiju 
    public GameObject slot;
    
   // True- ako se u stanju WAITING_TRACKABLE pojavi kutija, Event handler kutije postavlja ovu pormenljivu na true 
    private bool isPresent;

    private bool positionMatched;
    // Ime kutije koja je zadnja detektovana
    public string curBox;

    //Referenca na objekat koji je poslednji detektovan
    private GameObject curTrackable;

    //Dugme Next
    public Button btn;
    public Text btnText;

    //Tekst koji se prikazuje u INIT stanju da obavest korisnika da treba da pritisne na ekran da fiksira paletu
    public Text status;

    // Tekst koji se prikazuje kada je kutija postavljena na odgovarajuce mesto
    public Text curBoxName;

    // Za detekciju pritiska na dugme next
    private bool btnPressed;

    //Niz svih Trackable objekata 
    public GameObject[] trackables;

    //Referenca na Ground plane
    public GameObject plane;
    public GameObject planeFinder;

    //Lista svih Image Targeta odakle se dobija velicina kutije
    private XmlNodeList levelsList;

    //Za sada simulacija algoritma, sadrzi imena i Info za svaku kutiju
    private Dictionary<String, Info> alg;

    public GameObject basicPanel;
    public GameObject downPanel;

    public TextAsset xmlFile;
    private  bool groundFixed;
    /*Metod koji cita xml i dodaje sve ImageTargete u listu*/
    public void ReadXML()
    {
        XmlDocument xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
        xmlDoc.LoadXml(xmlFile.text);
       
        levelsList = xmlDoc.GetElementsByTagName("ImageTarget"); // array of the level nodes.

 

    }

    /*Method koji dohvata velicinu sve tri ivice kutije na osnovu njenog imena*/
    public Vector3 GetSizeFromXML(String name)
    {
        String nameRight = name + ".Right";
        String nameFront = name + ".Front";
        Vector3 size = new Vector3(0, 0, 0);
        foreach (XmlNode levelInfo in levelsList)
        {
            if (String.Equals(levelInfo.Attributes["name"].Value, nameRight))
            {
                String right = levelInfo.Attributes["size"].Value;
                String[] rights = right.Split(' ');
               // size.y = float.Parse(rights[0])/1000000;
                //size.z = float.Parse(rights[1]) / 1000000;
                size.y = float.Parse(rights[0]);
              size.z = float.Parse(rights[1]);
            }
            if (String.Equals(levelInfo.Attributes["name"].Value, nameFront))
            {
                String front = levelInfo.Attributes["size"].Value;
                String[] fronts = front.Split(' ');
               // size.x = float.Parse(fronts[0]) / 1000000;
                size.x = float.Parse(fronts[0]) ;
            }

        }
        return size;
    }

    /*Simulacija algoritma za sad*/
    private void FindBestFit()
    {
        alg = new Dictionary<string, Info>();

        Info k1 = new Info();
        k1.dimensions = GetSizeFromXML("k1");
        k1.detected = false;
        k1.position = new Vector3(0.189f, 0, -0.097f);
        alg.Add("k1", k1);

        Info k2 = new Info();
        k2.dimensions = GetSizeFromXML("k2");
        k2.detected = false;
        k2.position = new Vector3(0.058f, 0, 0.223f);
        alg.Add("k2", k2);



        Info k3 = new Info();
        k3.dimensions = GetSizeFromXML("k3");
        k3.detected = false;
        k3.position = new Vector3(0.165f, 0.076f, -0.035f);
        alg.Add("k3", k3);

        Info k4 = new Info();
        k4.dimensions = GetSizeFromXML("k4");
        k4.detected = false;
        k4.position = new Vector3(0.042f, 0.054f, 0.205f);
        alg.Add("k4", k4);

        /*Info k5 = new Info();
        k5.dimensions = GetSizeFromXML("k5");
        k5.detected = false;
        k5.position = new Vector3(0.024f, 0, - 0.121f);
        alg.Add("k5", k5);*/

        Info k6 = new Info();
        k6.dimensions = GetSizeFromXML("k6");
        k6.detected = false;
        k6.position = new Vector3(0.069f, 0, 0.067f);
        alg.Add("k6", k6);

        Info k7 = new Info();
        k7.dimensions = GetSizeFromXML("k7");
        k7.detected = false;
        k7.position = new Vector3(0.192f, 0, 0.093f);
        alg.Add("k7", k7);  
    }

    /* Metod koji na osnovu vektora koji sadrzi velicinu sve tri ivice, nalazi dve najvece i njih vraca kao dno kutije.*/
    private Vector2 GetBottom(Vector3 vect)
    {
        ArrayList arr = new ArrayList();
        arr.Add(vect.x);
        arr.Add(vect.y);
        arr.Add(vect.z);
        arr.Sort();
        return new Vector2((float)arr[2], (float)arr[1]);
    }
     /* Metod koji pozivaju handleri iz kutija onda kada je kutija detektovana*/
    public void RegisterFound(string obj)
    {
        if (state == State.WAITING_TRACKABLE)
        {
            if (alg[obj].detected == false)
            {
                isPresent = true;
                curBox = obj;
                alg[obj].detected = true;
            }
        }
    }

    public void RegisterCollision(String obj)
    {
        if (state == State.MATCHING_POSITION)
        {
            if (curBox.Equals(obj))
            {
                positionMatched = true;
            }
        }
    }

    /* Metod koji pozivaju handleri iz kutija onda kada je kutija izgubljena - za sad ne treba ali neka ga ovde*/
    public void RegisterLost(string obj)
    {
       
    }

    // Handler za click na dugme
    public void ButtonClicked()
    {
        btnPressed = true;
    }

    private bool isLevel1Finished()
    {
        foreach (KeyValuePair<string, Info> info in alg)
        {
           if (info.Value.position.y < 0.00001f)
            {
                if (info.Value.detected == false)
                {
                    return false;

                }
            }
        }
        return true;
    }



   

    void Start()
    {
        positionMatched = false;
        isPresent = false; // nijedna kutija nije detektovana
        this.ReadXML(); // iscitaj xml
       FindBestFit(); // pozivaj algoritam
        this.state = State.INIT; // pocetno stanje je init
       // this.btn.interactable = false; // sakrij dugme next
       // btnText.enabled = false;
        this.btnPressed = false; // dugme za sada nije pritisnuto
        this.curBoxName.text = "Current box: none";
        groundFixed = false;
        this.downPanel.SetActive(false);
    }

    public void RegisterGroundFixed()
    {
        groundFixed = true;
    }
    
    void Update()
    {
        /* inicijalno stanje u kome se ceka fiksiranje palete */
        if (state == State.INIT)
        {

          
                if (groundFixed==true)
                {
                //slot.transform.localScale = new Vector3(1, 1, 1);
                    state = State.WAITING_TRACKABLE;
                    this.status.text = "Scan box!";

            }
            
        }else if (state == State.WAITING_TRACKABLE) /* Ceka se detekcija neke kutije*/
        {

         //   Debug.Log("Waiting_Trackable");
            if (isPresent == true)
            {
                state = State.DETECTED;
                this.status.text = "Place box in correct position!";
    
                this.curBoxName.text = "Current box: "+curBox;
                isPresent = false;
            }
        }else if ( state== State.DETECTED) //Medjustanje da se zapamti kutija 
        {
            if (alg[curBox].position.y > 0)
            {
                if (isLevel1Finished() == false)
                {
                    this.status.text = "Level1 not completed, place another box.";
                    btnPressed = false;
                  //  this.btn.interactable = false;
                  //  btnText.enabled = false;
                    slot.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f); // Slot se smanjuje da bude nevidljiv            
                    this.slot.GetComponent<Renderer>().material = notPlaced;
                    state = State.WAITING_TRACKABLE;
                    alg[curBox].detected = false;
                    return;
                }
               
            }
            
          //  Debug.Log("Detected");

            slot.transform.parent = plane.transform; // ovo mozda moze samo u start
            slot.transform.localPosition = alg[curBox].position;
        
          //  Vector3 size = alg[curBox].dimensions; 
            slot.transform.localScale = alg[curBox].dimensions; //Postalvja se dimenzija slota na velicinu kutije

          //  this.status.text = "Pozicija " + slot.transform.localPosition.x + " " + slot.transform.localPosition.y + " " + slot.transform.localPosition.z + "Scale " + slot.transform.localScale.x + " " + slot.transform.localScale.y + " "  + slot.transform.localScale.z;


            for ( int i = 0; i < trackables.Length; i++) //prolazak kroz listu Trackable-a da se na osnovu imena pronadje onaj koji nama treba
            { 
                if (trackables[i]!=null && String.Equals(trackables[i].name, curBox))
                {
                    curTrackable = trackables[i];
                    break;
                }
            }            
            state = State.MATCHING_POSITION;
        }
        else if ( state== State.MATCHING_POSITION) // Stanje u kojem se ceka postalvjanje kutije na pravu poziciju
        {


            // Uslov za postalvjanje na odg poziciju, ovde se steluje odgovarajuca kolicina tolerancije prilikom spustanaj kutije na njen slot
            if(positionMatched)
            {
                positionMatched = false;
                state = State.NEXT_TRACKABLE;
              //  this.btn.interactable = true;//bagovalo dugme pa je ovo pomoglo
             //   btnText.enabled = true;

                this.basicPanel.SetActive(false);
                this.downPanel.SetActive(true);
                this.slot.GetComponent<Renderer>().material = placed;
            
                this.status.text = "Click button for next box!";
                this.curBoxName.text = "Current box: none";
            }
           

        } else if ( state== State.NEXT_TRACKABLE){ // Ceka se potvrda- pritisak na dugme next

          
            if (btnPressed == true) 
            {
               
                btnPressed = false;
              //  this.btn.interactable = false;
              //  btnText.enabled = false;

                this.basicPanel.SetActive(true);
                this.downPanel.SetActive(false);
                slot.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f); // Slot se smanjuje da bude nevidljiv 
                state = State.WAITING_TRACKABLE;
                this.status.text = "Scan box!";

                this.slot.GetComponent<Renderer>().material = notPlaced;
            }
        }

    }

    public void BackButtonClicked()
    {
        SceneManager.UnloadSceneAsync("NormalMode");
        SceneManager.LoadScene("MainMenu");
       
    }


    public void ExitButtonClicked()
    {
        Application.Quit();
    }
  
}
