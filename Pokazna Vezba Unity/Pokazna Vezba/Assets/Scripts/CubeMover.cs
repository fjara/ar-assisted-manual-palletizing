﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeMover : MonoBehaviour
{
    [SerializeField]
    private float speed = 1;
    [SerializeField]
    GameObject explosionPrefab;
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal"); // Strelica levo -1, Strelica desno 1
        float vertical = Input.GetAxis("Vertical"); // Strelica gore 1, Strelica dole -1
        Vector3 movement = new Vector3(horizontal, vertical);
        transform.position += movement*Time.deltaTime*speed;
       
    }

    private void OnCollisionEnter(Collision collision) { 
    

       /* if ("bomb".Equals(collision.gameObject.name))
            SceneManager.LoadSceneAsync("SampleScene");*/
   
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Object.Destroy(this.gameObject);
        Object.Destroy(collision.gameObject);

    }
}
