﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        float randomX = UnityEngine.Random.Range(-10, 10);
        float randomY = UnityEngine.Random.Range(10, 20);

        transform.position = new Vector3(randomX, randomY);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -10)
        {


            float randomX = UnityEngine.Random.Range(-10, 10);
            float randomY = UnityEngine.Random.Range(10, 20);

            transform.position= new Vector3(randomX,randomY);
            var rigidBody = transform.GetComponent<Rigidbody>();
            rigidBody.velocity = Vector3.zero;
        }
    }
}
